<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="example-small">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0">
            <attr name="layout">
                <string>38 104 36 36</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>161 68 90 90</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>344 104 104 36</string>
            </attr>
        </node>
        <node id="n9">
            <attr name="layout">
                <string>1013 90 114 54</string>
            </attr>
        </node>
        <node id="n10">
            <attr name="layout">
                <string>672 95 61 36</string>
            </attr>
        </node>
        <node id="n11">
            <attr name="layout">
                <string>609 215 87 72</string>
            </attr>
        </node>
        <node id="n13">
            <attr name="layout">
                <string>590 378 104 36</string>
            </attr>
        </node>
        <node id="n14">
            <attr name="layout">
                <string>734 382 104 36</string>
            </attr>
        </node>
        <node id="n15">
            <attr name="layout">
                <string>934 534 158 54</string>
            </attr>
        </node>
        <node id="n16">
            <attr name="layout">
                <string>667 490 87 36</string>
            </attr>
        </node>
        <node id="n17">
            <attr name="layout">
                <string>423 567 30 36</string>
            </attr>
        </node>
        <node id="n18">
            <attr name="layout">
                <string>559 692 101 90</string>
            </attr>
        </node>
        <node id="n19">
            <attr name="layout">
                <string>782 727 104 36</string>
            </attr>
        </node>
        <node id="n12">
            <attr name="layout">
                <string>727 219 87 72</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>77 229 81 108</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>200 226 81 108</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>358 240 66 72</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>1208 16 85 108</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>1209 155 81 108</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>480 200 81 108</string>
            </attr>
        </node>
        <node id="n20">
            <attr name="layout">
                <string>860 198 81 108</string>
            </attr>
        </node>
        <node id="n21">
            <attr name="layout">
                <string>442 371 85 72</string>
            </attr>
        </node>
        <node id="n22">
            <attr name="layout">
                <string>905 370 85 72</string>
            </attr>
        </node>
        <node id="n23">
            <attr name="layout">
                <string>1181 450 85 108</string>
            </attr>
        </node>
        <node id="n24">
            <attr name="layout">
                <string>1185 605 81 108</string>
            </attr>
        </node>
        <node id="n25">
            <attr name="layout">
                <string>340 799 85 108</string>
            </attr>
        </node>
        <node id="n26">
            <attr name="layout">
                <string>354 660 81 108</string>
            </attr>
        </node>
        <node id="n27">
            <attr name="layout">
                <string>977 711 85 72</string>
            </attr>
        </node>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n0" to="n1">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>flag:PLUS_expression</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:result = ""</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:typeResult = ""</string>
            </attr>
        </edge>
        <edge from="n1" to="n2">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n1" to="n3">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n1" to="n4">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n2" to="n10">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n2" to="n5">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>flag:CONDITION</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>flag:GREATER_expression</string>
            </attr>
        </edge>
        <edge from="n9" to="n6">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n9" to="n7">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>flag:IF_selection</string>
            </attr>
        </edge>
        <edge from="n10" to="n11">
            <attr name="label">
                <string>true</string>
            </attr>
        </edge>
        <edge from="n10" to="n9">
            <attr name="label">
                <string>condition</string>
            </attr>
        </edge>
        <edge from="n10" to="n12">
            <attr name="label">
                <string>false</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>let:result = ""</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>let:typeResult = ""</string>
            </attr>
        </edge>
        <edge from="n11" to="n13">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n11" to="n8">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n13" to="n16">
            <attr name="label">
                <string>next</string>
            </attr>
            <attr name="layout">
                <string>501 1 903 425 985 566 11</string>
            </attr>
        </edge>
        <edge from="n13" to="n21">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n14" to="n16">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n14" to="n22">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>flag:CONDITION</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>flag:GREATER_EQUAL_expression</string>
            </attr>
        </edge>
        <edge from="n15" to="n23">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n15" to="n24">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>flag:WHILE_iteration</string>
            </attr>
        </edge>
        <edge from="n16" to="n18">
            <attr name="label">
                <string>true</string>
            </attr>
        </edge>
        <edge from="n16" to="n17">
            <attr name="label">
                <string>false</string>
            </attr>
        </edge>
        <edge from="n16" to="n15">
            <attr name="label">
                <string>condition</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>flag:MINUS_expression</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>let:result = ""</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>let:typeResult = ""</string>
            </attr>
        </edge>
        <edge from="n18" to="n19">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n18" to="n26">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n18" to="n25">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n19" to="n16">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n19" to="n27">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>let:result = ""</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>let:typeResult = ""</string>
            </attr>
        </edge>
        <edge from="n12" to="n14">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n12" to="n20">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:type = "int"</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:value = "1"</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:type = "int"</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:value = "2"</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:type = "string"</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:value = "a"</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:type = "string"</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:value = "a"</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:type = "int"</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:value = "2"</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:type = "int"</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:value = "1"</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>let:type = "int"</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>let:value = "2"</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>let:type = "string"</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>let:value = "b"</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>let:type = "string"</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>let:value = "b"</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>let:type = "string"</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>let:value = "a"</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:type = "int"</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:value = "1"</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:type = "string"</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:value = "a"</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:realType = ""</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:realValue = ""</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:type = "int"</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:value = "1"</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>let:type = "string"</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>let:value = "a"</string>
            </attr>
        </edge>
    </graph>
</gxl>
