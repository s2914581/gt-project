<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="example-function">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n113">
            <attr name="layout">
                <string>97 8 36 36</string>
            </attr>
        </node>
        <node id="n116">
            <attr name="layout">
                <string>88 396 30 36</string>
            </attr>
        </node>
        <node id="n118">
            <attr name="layout">
                <string>61 249 94 90</string>
            </attr>
        </node>
        <node id="n121">
            <attr name="layout">
                <string>61 103 94 90</string>
            </attr>
        </node>
        <node id="n129">
            <attr name="layout">
                <string>354 14 102 54</string>
            </attr>
        </node>
        <node id="n130">
            <attr name="layout">
                <string>364 124 83 90</string>
            </attr>
        </node>
        <node id="n131">
            <attr name="layout">
                <string>350 266 97 54</string>
            </attr>
        </node>
        <node id="n133">
            <attr name="layout">
                <string>635 8 102 54</string>
            </attr>
        </node>
        <node id="n134">
            <attr name="layout">
                <string>647 109 83 72</string>
            </attr>
        </node>
        <node id="n135">
            <attr name="layout">
                <string>817 21 69 54</string>
            </attr>
        </node>
        <node id="n136">
            <attr name="layout">
                <string>635 232 97 54</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>196 242 81 108</string>
            </attr>
        </node>
        <node id="n143">
            <attr name="layout">
                <string>492 118 81 108</string>
            </attr>
        </node>
        <node id="n146">
            <attr name="layout">
                <string>787 98 81 108</string>
            </attr>
        </node>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n113" to="n121">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>flag:FUNCTION_CALL</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:name = string:"func2"</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n118" to="n116">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n118" to="n140">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>flag:FUNCTION_CALL</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:name = string:"func1"</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n121" to="n118">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>flag:FUNCTION_START</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>let:name = string:"func1"</string>
            </attr>
        </edge>
        <edge from="n129" to="n130">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>flag:RETURN</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:name = string:""</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n130" to="n143">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n130" to="n131">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>flag:FUNCTION_STOP</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:name = string:"func1"</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>flag:FUNCTION_START</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>let:name = string:"func2"</string>
            </attr>
        </edge>
        <edge from="n133" to="n134">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n133" to="n135">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>flag:RETURN</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n134" to="n136">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n134" to="n146">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>flag:PARAMETER</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>let:name = string:"a"</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>flag:FUNCTION_STOP</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>let:name = string:"func2"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:value = string:"5"</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
    </graph>
</gxl>
