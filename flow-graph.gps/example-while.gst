<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="s19">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n109">
            <attr name="layout">
                <string>84 438 30 36</string>
            </attr>
        </node>
        <node id="n110">
            <attr name="layout">
                <string>306 100 87 72</string>
            </attr>
        </node>
        <node id="n111">
            <attr name="layout">
                <string>331 8 36 36</string>
            </attr>
        </node>
        <node id="n114">
            <attr name="layout">
                <string>307 320 87 72</string>
            </attr>
        </node>
        <node id="n119">
            <attr name="layout">
                <string>298 228 104 36</string>
            </attr>
        </node>
        <node id="n120">
            <attr name="layout">
                <string>159 205 85 72</string>
            </attr>
        </node>
        <node id="n121">
            <attr name="layout">
                <string>454 91 81 108</string>
            </attr>
        </node>
        <node id="n123">
            <attr name="layout">
                <string>413 527 101 90</string>
            </attr>
        </node>
        <node id="n124">
            <attr name="layout">
                <string>656 334 158 54</string>
            </attr>
        </node>
        <node id="n130">
            <attr name="layout">
                <string>606 173 85 108</string>
            </attr>
        </node>
        <node id="n131">
            <attr name="layout">
                <string>762 177 81 108</string>
            </attr>
        </node>
        <node id="n136">
            <attr name="layout">
                <string>229 559 104 36</string>
            </attr>
        </node>
        <node id="n137">
            <attr name="layout">
                <string>91 545 85 72</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>565 445 85 108</string>
            </attr>
        </node>
        <node id="n141">
            <attr name="layout">
                <string>567 585 81 108</string>
            </attr>
        </node>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n110" to="n121">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n110" to="n119">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n111" to="n110">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n114" to="n123">
            <attr name="label">
                <string>true</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>flag:WHILE_iteration</string>
            </attr>
        </edge>
        <edge from="n114" to="n109">
            <attr name="label">
                <string>false</string>
            </attr>
        </edge>
        <edge from="n114" to="n124">
            <attr name="label">
                <string>condition</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n119" to="n114">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n119" to="n120">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:value = string:"3"</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n123" to="n136">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>flag:MINUS_expression</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n123" to="n141">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n123" to="n140">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>flag:CONDITION</string>
            </attr>
        </edge>
        <edge from="n124" to="n131">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n124" to="n130">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>flag:GREATER_EQUAL_expression</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n136" to="n114">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n136" to="n137">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
    </graph>
</gxl>
