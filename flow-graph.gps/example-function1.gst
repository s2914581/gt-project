<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="example-function1">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n113">
            <attr name="layout">
                <string>274 14 36 36</string>
            </attr>
        </node>
        <node id="n116">
            <attr name="layout">
                <string>245 516 30 36</string>
            </attr>
        </node>
        <node id="n118">
            <attr name="layout">
                <string>218 369 94 90</string>
            </attr>
        </node>
        <node id="n133">
            <attr name="layout">
                <string>635 8 102 54</string>
            </attr>
        </node>
        <node id="n134">
            <attr name="layout">
                <string>647 109 83 72</string>
            </attr>
        </node>
        <node id="n135">
            <attr name="layout">
                <string>817 21 69 54</string>
            </attr>
        </node>
        <node id="n136">
            <attr name="layout">
                <string>635 232 97 54</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>353 362 81 108</string>
            </attr>
        </node>
        <node id="n146">
            <attr name="layout">
                <string>787 98 81 108</string>
            </attr>
        </node>
        <node id="n0">
            <attr name="layout">
                <string>237 104 90 90</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>105 98 81 108</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>367 94 81 108</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>206 248 104 36</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>67 233 85 72</string>
            </attr>
        </node>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n113" to="n0">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>flag:FUNCTION_CALL</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:name = string:"func2"</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n118" to="n116">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n118" to="n140">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>flag:FUNCTION_START</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>let:name = string:"func2"</string>
            </attr>
        </edge>
        <edge from="n133" to="n134">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n133" to="n135">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>flag:RETURN</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n134" to="n136">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n134" to="n146">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>flag:PARAMETER</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>let:name = string:"a"</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>flag:FUNCTION_STOP</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>let:name = string:"func2"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>flag:PLUS_expression</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n0" to="n1">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n0" to="n2">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n0" to="n3">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n3" to="n4">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n3" to="n118">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
    </graph>
</gxl>
