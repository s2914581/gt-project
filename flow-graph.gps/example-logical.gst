<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="example-logical">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n76">
            <attr name="layout">
                <string>312 804 30 36</string>
            </attr>
        </node>
        <node id="n77">
            <attr name="layout">
                <string>284 100 89 90</string>
            </attr>
        </node>
        <node id="n78">
            <attr name="layout">
                <string>310 8 36 36</string>
            </attr>
        </node>
        <node id="n81">
            <attr name="layout">
                <string>288 570 89 90</string>
            </attr>
        </node>
        <node id="n84">
            <attr name="layout">
                <string>286 338 87 90</string>
            </attr>
        </node>
        <node id="n89">
            <attr name="layout">
                <string>278 719 104 36</string>
            </attr>
        </node>
        <node id="n90">
            <attr name="layout">
                <string>123 699 85 72</string>
            </attr>
        </node>
        <node id="n92">
            <attr name="layout">
                <string>425 557 81 108</string>
            </attr>
        </node>
        <node id="n95">
            <attr name="layout">
                <string>278 486 104 36</string>
            </attr>
        </node>
        <node id="n96">
            <attr name="layout">
                <string>147 465 85 72</string>
            </attr>
        </node>
        <node id="n99">
            <attr name="layout">
                <string>445 322 84 108</string>
            </attr>
        </node>
        <node id="n100">
            <attr name="layout">
                <string>151 326 81 108</string>
            </attr>
        </node>
        <node id="n103">
            <attr name="layout">
                <string>277 246 104 36</string>
            </attr>
        </node>
        <node id="n104">
            <attr name="layout">
                <string>146 226 85 72</string>
            </attr>
        </node>
        <node id="n107">
            <attr name="layout">
                <string>150 96 84 108</string>
            </attr>
        </node>
        <node id="n108">
            <attr name="layout">
                <string>426 99 81 108</string>
            </attr>
        </node>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>flag:AND_expression</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>flag:BOOL_value</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n77" to="n103">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n77" to="n108">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n77" to="n107">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n78" to="n77">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>flag:BOOL_value</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>flag:NOT_expression</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n81" to="n89">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n81" to="n92">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>flag:BOOL_value</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>flag:OR_expression</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n84" to="n99">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n84" to="n100">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n84" to="n95">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n89" to="n89">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n89" to="n89">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n89" to="n76">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n89" to="n90">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>let:value = string:"n"</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>let:type = string:"bool"</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>let:value = string:"true"</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n95" to="n96">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n95" to="n81">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>let:value = string:"m"</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>let:type = string:"bool"</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>let:value = string:"false"</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:type = string:"bool"</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:value = string:"true"</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n103" to="n104">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n103" to="n84">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>let:value = string:"l"</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>let:type = string:"bool"</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>let:value = string:"false"</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>let:type = string:"bool"</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>let:value = string:"false"</string>
            </attr>
        </edge>
    </graph>
</gxl>
