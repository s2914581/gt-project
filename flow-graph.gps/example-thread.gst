<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="s22">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n93">
            <attr name="layout">
                <string>452 754 30 36</string>
            </attr>
        </node>
        <node id="n94">
            <attr name="layout">
                <string>431 8 36 36</string>
            </attr>
        </node>
        <node id="n96">
            <attr name="layout">
                <string>404 100 90 90</string>
            </attr>
        </node>
        <node id="n99">
            <attr name="layout">
                <string>409 488 90 90</string>
            </attr>
        </node>
        <node id="n102">
            <attr name="layout">
                <string>389 338 120 72</string>
            </attr>
        </node>
        <node id="n106">
            <attr name="layout">
                <string>269 226 85 72</string>
            </attr>
        </node>
        <node id="n107">
            <attr name="layout">
                <string>397 246 104 36</string>
            </attr>
        </node>
        <node id="n110">
            <attr name="layout">
                <string>268 91 81 108</string>
            </attr>
        </node>
        <node id="n111">
            <attr name="layout">
                <string>549 93 81 108</string>
            </attr>
        </node>
        <node id="n114">
            <attr name="layout">
                <string>275 637 85 72</string>
            </attr>
        </node>
        <node id="n115">
            <attr name="layout">
                <string>415 658 104 36</string>
            </attr>
        </node>
        <node id="n118">
            <attr name="layout">
                <string>262 477 85 108</string>
            </attr>
        </node>
        <node id="n119">
            <attr name="layout">
                <string>544 480 81 108</string>
            </attr>
        </node>
        <node id="n120">
            <attr name="layout">
                <string>950 667 30 36</string>
            </attr>
        </node>
        <node id="n122">
            <attr name="layout">
                <string>907 420 90 90</string>
            </attr>
        </node>
        <node id="n127">
            <attr name="layout">
                <string>770 554 85 72</string>
            </attr>
        </node>
        <node id="n128">
            <attr name="layout">
                <string>906 576 104 36</string>
            </attr>
        </node>
        <node id="n131">
            <attr name="layout">
                <string>759 411 81 108</string>
            </attr>
        </node>
        <node id="n132">
            <attr name="layout">
                <string>1051 408 81 108</string>
            </attr>
        </node>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n94" to="n96">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n96" to="n111">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>flag:PLUS_expression</string>
            </attr>
        </edge>
        <edge from="n96" to="n107">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n96" to="n110">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n99" to="n119">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n99" to="n118">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>flag:PLUS_expression</string>
            </attr>
        </edge>
        <edge from="n99" to="n115">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>flag:THREAD_concurrency</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n102" to="n99">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n102" to="n122">
            <attr name="label">
                <string>run</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n107" to="n106">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n107" to="n102">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n115" to="n114">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n115" to="n93">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>flag:PLUS_expression</string>
            </attr>
        </edge>
        <edge from="n122" to="n131">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n122" to="n128">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n122" to="n132">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>let:value = string:"b"</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n128" to="n127">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n128" to="n120">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>let:value = string:"3"</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
    </graph>
</gxl>
