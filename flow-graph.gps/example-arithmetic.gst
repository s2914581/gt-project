<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="example-arithmetic">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n129">
            <attr name="layout">
                <string>369 1254 30 36</string>
            </attr>
        </node>
        <node id="n130">
            <attr name="layout">
                <string>357 103 95 90</string>
            </attr>
        </node>
        <node id="n131">
            <attr name="layout">
                <string>388 8 36 36</string>
            </attr>
        </node>
        <node id="n134">
            <attr name="layout">
                <string>350 1032 88 90</string>
            </attr>
        </node>
        <node id="n137">
            <attr name="layout">
                <string>352 803 88 90</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>355 561 91 90</string>
            </attr>
        </node>
        <node id="n143">
            <attr name="layout">
                <string>354 338 105 90</string>
            </attr>
        </node>
        <node id="n148">
            <attr name="layout">
                <string>339 1178 104 36</string>
            </attr>
        </node>
        <node id="n149">
            <attr name="layout">
                <string>192 1159 86 72</string>
            </attr>
        </node>
        <node id="n152">
            <attr name="layout">
                <string>198 1029 80 108</string>
            </attr>
        </node>
        <node id="n153">
            <attr name="layout">
                <string>497 1027 81 108</string>
            </attr>
        </node>
        <node id="n156">
            <attr name="layout">
                <string>345 949 104 36</string>
            </attr>
        </node>
        <node id="n157">
            <attr name="layout">
                <string>201 928 86 72</string>
            </attr>
        </node>
        <node id="n160">
            <attr name="layout">
                <string>198 796 80 108</string>
            </attr>
        </node>
        <node id="n161">
            <attr name="layout">
                <string>494 790 81 108</string>
            </attr>
        </node>
        <node id="n164">
            <attr name="layout">
                <string>344 716 104 36</string>
            </attr>
        </node>
        <node id="n165">
            <attr name="layout">
                <string>196 702 86 72</string>
            </attr>
        </node>
        <node id="n168">
            <attr name="layout">
                <string>217 552 80 108</string>
            </attr>
        </node>
        <node id="n169">
            <attr name="layout">
                <string>504 551 81 108</string>
            </attr>
        </node>
        <node id="n172">
            <attr name="layout">
                <string>346 485 104 36</string>
            </attr>
        </node>
        <node id="n173">
            <attr name="layout">
                <string>211 459 86 72</string>
            </attr>
        </node>
        <node id="n176">
            <attr name="layout">
                <string>221 313 80 108</string>
            </attr>
        </node>
        <node id="n177">
            <attr name="layout">
                <string>506 324 81 108</string>
            </attr>
        </node>
        <node id="n180">
            <attr name="layout">
                <string>354 246 104 36</string>
            </attr>
        </node>
        <node id="n181">
            <attr name="layout">
                <string>211 220 86 72</string>
            </attr>
        </node>
        <node id="n184">
            <attr name="layout">
                <string>216 87 80 108</string>
            </attr>
        </node>
        <node id="n185">
            <attr name="layout">
                <string>513 105 81 108</string>
            </attr>
        </node>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>flag:PLUS_expression</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n130" to="n185">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n130" to="n180">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n130" to="n184">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n131" to="n130">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>flag:EXP_expression</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n134" to="n148">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n134" to="n153">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n134" to="n152">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>flag:DIV_expression</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n137" to="n161">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n137" to="n160">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n137" to="n156">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>flag:MUL_expression</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n164">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n140" to="n169">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n140" to="n168">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>flag:MINUS_expression</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n143" to="n176">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n143" to="n172">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n143" to="n177">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n148" to="n149">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n148" to="n129">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:value = string:"e"</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>let:value = string:"3"</string>
            </attr>
        </edge>
        <edge from="n156" to="n156">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n156" to="n156">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n156" to="n157">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n156" to="n134">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:value = string:"d"</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:value = string:"6"</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>let:value = string:"3"</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n164" to="n137">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n164" to="n165">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:value = string:"c"</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n172" to="n173">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n172" to="n140">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>let:value = string:"b"</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:value = string:"5"</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:value = string:"3"</string>
            </attr>
        </edge>
        <edge from="n180" to="n180">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n180" to="n180">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n180" to="n181">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n180" to="n143">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
    </graph>
</gxl>
