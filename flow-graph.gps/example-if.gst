<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="s35">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n139">
            <attr name="layout">
                <string>305 803 30 36</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>700 100 87 72</string>
            </attr>
        </node>
        <node id="n141">
            <attr name="layout">
                <string>725 8 36 36</string>
            </attr>
        </node>
        <node id="n144">
            <attr name="layout">
                <string>475 402 87 72</string>
            </attr>
        </node>
        <node id="n147">
            <attr name="layout">
                <string>701 320 87 72</string>
            </attr>
        </node>
        <node id="n156">
            <attr name="layout">
                <string>692 228 104 36</string>
            </attr>
        </node>
        <node id="n157">
            <attr name="layout">
                <string>548 206 85 72</string>
            </attr>
        </node>
        <node id="n158">
            <attr name="layout">
                <string>833 77 81 108</string>
            </attr>
        </node>
        <node id="n152">
            <attr name="layout">
                <string>959 448 87 72</string>
            </attr>
        </node>
        <node id="n153">
            <attr name="layout">
                <string>1182 344 101 54</string>
            </attr>
        </node>
        <node id="n164">
            <attr name="layout">
                <string>1094 181 85 108</string>
            </attr>
        </node>
        <node id="n165">
            <attr name="layout">
                <string>1263 180 81 108</string>
            </attr>
        </node>
        <node id="n167">
            <attr name="layout">
                <string>217 426 114 54</string>
            </attr>
        </node>
        <node id="n169">
            <attr name="layout">
                <string>280 558 87 72</string>
            </attr>
        </node>
        <node id="n170">
            <attr name="layout">
                <string>456 592 87 72</string>
            </attr>
        </node>
        <node id="n176">
            <attr name="layout">
                <string>154 262 85 108</string>
            </attr>
        </node>
        <node id="n177">
            <attr name="layout">
                <string>312 265 81 108</string>
            </attr>
        </node>
        <node id="n187">
            <attr name="layout">
                <string>872 602 104 36</string>
            </attr>
        </node>
        <node id="n188">
            <attr name="layout">
                <string>1043 582 85 72</string>
            </attr>
        </node>
        <node id="n189">
            <attr name="layout">
                <string>1102 430 81 108</string>
            </attr>
        </node>
        <node id="n191">
            <attr name="layout">
                <string>448 738 104 36</string>
            </attr>
        </node>
        <node id="n192">
            <attr name="layout">
                <string>601 717 85 72</string>
            </attr>
        </node>
        <node id="n193">
            <attr name="layout">
                <string>602 573 81 108</string>
            </attr>
        </node>
        <node id="n195">
            <attr name="layout">
                <string>265 690 104 36</string>
            </attr>
        </node>
        <node id="n196">
            <attr name="layout">
                <string>126 671 85 72</string>
            </attr>
        </node>
        <node id="n197">
            <attr name="layout">
                <string>126 541 81 108</string>
            </attr>
        </node>
        <edge from="n139" to="n139">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n139" to="n139">
            <attr name="label">
                <string>flag:STOP</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n140" to="n158">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n140" to="n156">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n141" to="n140">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>flag:START</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n144" to="n169">
            <attr name="label">
                <string>false</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>flag:IF_selection</string>
            </attr>
        </edge>
        <edge from="n144" to="n170">
            <attr name="label">
                <string>true</string>
            </attr>
        </edge>
        <edge from="n144" to="n167">
            <attr name="label">
                <string>condition</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n147" to="n153">
            <attr name="label">
                <string>condition</string>
            </attr>
        </edge>
        <edge from="n147" to="n152">
            <attr name="label">
                <string>true</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>flag:IF_selection</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n147" to="n144">
            <attr name="label">
                <string>false</string>
            </attr>
        </edge>
        <edge from="n156" to="n156">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n156" to="n147">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n156" to="n157">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n156" to="n156">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>let:value = string:"3"</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n152" to="n189">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n152" to="n187">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n153" to="n165">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>flag:EQUAL_expression</string>
            </attr>
        </edge>
        <edge from="n153" to="n164">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>flag:CONDITION</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:value = string:"3"</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n167" to="n167">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n167" to="n167">
            <attr name="label">
                <string>flag:GREATER_expression</string>
            </attr>
        </edge>
        <edge from="n167" to="n176">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n167" to="n177">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n167" to="n167">
            <attr name="label">
                <string>flag:CONDITION</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n169" to="n195">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n169" to="n197">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>let:typeResult = string:""</string>
            </attr>
        </edge>
        <edge from="n170" to="n191">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n170" to="n193">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>flag:INT_value</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>let:result = string:""</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:value = string:"a"</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n187" to="n187">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n187" to="n187">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n187" to="n144">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n187" to="n188">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n188" to="n188">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n188" to="n188">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n188" to="n188">
            <attr name="label">
                <string>let:value = string:"b"</string>
            </attr>
        </edge>
        <edge from="n188" to="n188">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>let:value = string:"0"</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n191" to="n192">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n191" to="n139">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>let:value = string:"b"</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>let:value = string:"1"</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n195" to="n195">
            <attr name="label">
                <string>type:Elem</string>
            </attr>
        </edge>
        <edge from="n195" to="n195">
            <attr name="label">
                <string>flag:ASSIGN_expression</string>
            </attr>
        </edge>
        <edge from="n195" to="n139">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n195" to="n196">
            <attr name="label">
                <string>has</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>flag:LEFT</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>let:value = string:"b"</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>let:type = string:"string"</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>type:Operand</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>flag:RIGHT</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:type = string:"int"</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:value = string:"2"</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:realValue = string:""</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:realType = string:""</string>
            </attr>
        </edge>
    </graph>
</gxl>
