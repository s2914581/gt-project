<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="example-while">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0">
            <attr name="layout">
                <string>50 688 55 72</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>67 596 21 36</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>169 596 57 36</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>308 798 55 72</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>322 688 27 54</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>307 596 57 36</string>
            </attr>
        </node>
        <node id="n9">
            <attr name="layout">
                <string>160 486 93 54</string>
            </attr>
        </node>
        <node id="n10">
            <attr name="layout">
                <string>186 376 41 54</string>
            </attr>
        </node>
        <node id="n11">
            <attr name="layout">
                <string>174 284 66 36</string>
            </attr>
        </node>
        <node id="n12">
            <attr name="layout">
                <string>333 486 43 90</string>
            </attr>
        </node>
        <node id="n14">
            <attr name="layout">
                <string>344 376 22 54</string>
            </attr>
        </node>
        <node id="n15">
            <attr name="layout">
                <string>320 284 69 36</string>
            </attr>
        </node>
        <node id="n16">
            <attr name="layout">
                <string>175 192 90 36</string>
            </attr>
        </node>
        <node id="n17">
            <attr name="layout">
                <string>426 376 77 54</string>
            </attr>
        </node>
        <node id="n19">
            <attr name="layout">
                <string>446 872 55 72</string>
            </attr>
        </node>
        <node id="n20">
            <attr name="layout">
                <string>463 762 21 54</string>
            </attr>
        </node>
        <node id="n21">
            <attr name="layout">
                <string>445 652 57 54</string>
            </attr>
        </node>
        <node id="n22">
            <attr name="layout">
                <string>444 560 60 36</string>
            </attr>
        </node>
        <node id="n23">
            <attr name="layout">
                <string>584 560 103 36</string>
            </attr>
        </node>
        <node id="n25">
            <attr name="layout">
                <string>770 872 55 72</string>
            </attr>
        </node>
        <node id="n27">
            <attr name="layout">
                <string>784 762 27 54</string>
            </attr>
        </node>
        <node id="n28">
            <attr name="layout">
                <string>769 652 57 54</string>
            </attr>
        </node>
        <node id="n29">
            <attr name="layout">
                <string>768 560 60 36</string>
            </attr>
        </node>
        <node id="n30">
            <attr name="layout">
                <string>602 450 68 54</string>
            </attr>
        </node>
        <node id="n31">
            <attr name="layout">
                <string>584 376 103 18</string>
            </attr>
        </node>
        <node id="n32">
            <attr name="layout">
                <string>757 450 43 90</string>
            </attr>
        </node>
        <node id="n33">
            <attr name="layout">
                <string>768 376 22 18</string>
            </attr>
        </node>
        <node id="n34">
            <attr name="layout">
                <string>869 964 55 72</string>
            </attr>
        </node>
        <node id="n35">
            <attr name="layout">
                <string>886 872 21 36</string>
            </attr>
        </node>
        <node id="n36">
            <attr name="layout">
                <string>988 872 57 36</string>
            </attr>
        </node>
        <node id="n37">
            <attr name="layout">
                <string>991 1166 55 72</string>
            </attr>
        </node>
        <node id="n38">
            <attr name="layout">
                <string>1008 1056 21 54</string>
            </attr>
        </node>
        <node id="n39">
            <attr name="layout">
                <string>990 964 57 36</string>
            </attr>
        </node>
        <node id="n40">
            <attr name="layout">
                <string>1128 964 53 36</string>
            </attr>
        </node>
        <node id="n42">
            <attr name="layout">
                <string>1263 1166 55 72</string>
            </attr>
        </node>
        <node id="n43">
            <attr name="layout">
                <string>1277 1056 27 54</string>
            </attr>
        </node>
        <node id="n44">
            <attr name="layout">
                <string>1262 964 57 36</string>
            </attr>
        </node>
        <node id="n45">
            <attr name="layout">
                <string>1126 872 57 36</string>
            </attr>
        </node>
        <node id="n46">
            <attr name="layout">
                <string>1047 762 93 54</string>
            </attr>
        </node>
        <node id="n47">
            <attr name="layout">
                <string>1073 652 41 54</string>
            </attr>
        </node>
        <node id="n48">
            <attr name="layout">
                <string>1061 560 66 36</string>
            </attr>
        </node>
        <node id="n49">
            <attr name="layout">
                <string>1220 762 43 90</string>
            </attr>
        </node>
        <node id="n50">
            <attr name="layout">
                <string>1231 652 22 54</string>
            </attr>
        </node>
        <node id="n51">
            <attr name="layout">
                <string>1207 560 69 36</string>
            </attr>
        </node>
        <node id="n52">
            <attr name="layout">
                <string>1015 450 158 54</string>
            </attr>
        </node>
        <node id="n53">
            <attr name="layout">
                <string>1044 376 100 18</string>
            </attr>
        </node>
        <node id="n54">
            <attr name="layout">
                <string>1224 376 70 54</string>
            </attr>
        </node>
        <node id="n56">
            <attr name="layout">
                <string>820 266 103 54</string>
            </attr>
        </node>
        <node id="n57">
            <attr name="layout">
                <string>839 192 66 18</string>
            </attr>
        </node>
        <node id="n58">
            <attr name="layout">
                <string>1353 394 43 90</string>
            </attr>
        </node>
        <node id="n59">
            <attr name="layout">
                <string>1364 284 22 54</string>
            </attr>
        </node>
        <node id="n60">
            <attr name="layout">
                <string>1340 192 69 36</string>
            </attr>
        </node>
        <node id="n61">
            <attr name="layout">
                <string>685 82 90 54</string>
            </attr>
        </node>
        <node id="n62">
            <attr name="layout">
                <string>715 8 29 18</string>
            </attr>
        </node>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:ID</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:text = string:"a"</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Id_</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n2" to="n0">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n2" to="n3">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:ASSIGN</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:text = string:"="</string>
            </attr>
        </edge>
        <edge from="n3" to="n8">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>type:INT</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:text = string:"3"</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:Int_t</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n7" to="n5">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>type:Int_result</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n8" to="n7">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>type:Int_assignment</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n9" to="n8">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n9" to="n2">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n9" to="n3">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>type:Rvalue</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n10" to="n9">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>type:Expression</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n11" to="n15">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n11" to="n10">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>type:CRLF</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>let:text = string:"&#13;
"</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>type:Crlf</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n14" to="n12">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>type:Terminator</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n15" to="n14">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>type:Expression_list</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n16" to="n15">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n16" to="n11">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n16" to="n57">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>type:WHILE</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>let:text = string:"while"</string>
            </attr>
        </edge>
        <edge from="n17" to="n31">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>type:ID</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>let:text = string:"a"</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>type:Id_</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n20" to="n19">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>type:Int_result</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n21" to="n20">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>type:Comp_var</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n22" to="n21">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n22" to="n23">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>type:GREATER_EQUAL</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>let:text = string:"&gt;="</string>
            </attr>
        </edge>
        <edge from="n23" to="n29">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>type:INT</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:text = string:"1"</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>type:Int_t</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n27" to="n25">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>type:Int_result</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n28" to="n27">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>type:Comp_var</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n29" to="n28">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n30" to="n22">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n30" to="n29">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n30" to="n23">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>type:Cond_expression</string>
            </attr>
        </edge>
        <edge from="n31" to="n33">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n31" to="n30">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>type:CRLF</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>let:text = string:"&#13;
"</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>type:Crlf</string>
            </attr>
        </edge>
        <edge from="n33" to="n32">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n33" to="n53">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>type:ID</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>let:text = string:"a"</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>type:Id_</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n35" to="n34">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n35" to="n36">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>type:ASSIGN</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>let:text = string:"="</string>
            </attr>
        </edge>
        <edge from="n36" to="n45">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>type:ID</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>let:text = string:"a"</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>type:Id_</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n38" to="n37">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>type:Int_result</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n39" to="n40">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n39" to="n38">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>type:MINUS</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>let:text = string:"-"</string>
            </attr>
        </edge>
        <edge from="n40" to="n44">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>type:INT</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>let:text = string:"1"</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>type:Int_t</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n43" to="n42">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>type:Int_result</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n44" to="n43">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>type:Int_result</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n45" to="n44">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n45" to="n39">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n45" to="n40">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>type:Int_assignment</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n46" to="n45">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n46" to="n35">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n46" to="n36">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>type:Rvalue</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n47" to="n46">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>type:Expression</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n48" to="n51">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n48" to="n47">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>type:CRLF</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>let:text = string:"&#13;
"</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>type:Crlf</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n50" to="n49">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>type:Terminator</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n51" to="n50">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>type:Statement_expression_list</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n52" to="n48">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n52" to="n51">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>type:Statement_body</string>
            </attr>
        </edge>
        <edge from="n53" to="n52">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n53" to="n54">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>type:END</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>let:text = string:"end"</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>type:While_statement</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n56" to="n17">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n56" to="n33">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n56" to="n53">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n56" to="n54">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n56" to="n31">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>type:Expression</string>
            </attr>
        </edge>
        <edge from="n57" to="n60">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n57" to="n56">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>type:CRLF</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>let:text = string:"&#13;
"</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>type:Crlf</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n59" to="n58">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>type:Terminator</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n60" to="n59">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>type:Expression_list</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n61" to="n16">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n61" to="n57">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n61" to="n60">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>type:Prog</string>
            </attr>
        </edge>
        <edge from="n62" to="n61">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
    </graph>
</gxl>
