Credits to Original Source: https://github.com/antlr/grammars-v4/tree/master/ruby

# Development Guide
1. Make edits on ANTLR grammar in `Ruby.g4` file (note: must always end with a new line)
2. Generate ANTLR lexer and parser `java -cp gt-parser.jar org.antlr.v4.Tool Ruby.g4`
3. Compile generated files `javac -cp gt-parser.jar *.java`
4. Update sample Ruby program in the `example.rb` file
5. Generate AST graph `java -cp ".;gt-parser.jar" nl.utwente.gt.Parse Ruby -out example.gps -type -graph example.rb`
6. Check AST graph in the `example.gps` file