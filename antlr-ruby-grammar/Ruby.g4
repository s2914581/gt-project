/*
 * [The "BSD license"]
 *  Copyright (c) 2014 Alexander Belov
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 *  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  A grammar for Ruby-like language written in ANTLR v4.
 *  You can find compiler into Parrot VM intermediate representation language
 *  (PIR) here: https://github.com/AlexBelov/corundum
 */

grammar Ruby;

prog : expression_list EOF;

expression_list : expression terminator
                | expression_list expression terminator
                | terminator
                ;

expression : function_definition
           | function_call
           | return_statement
           | rvalue
           | if_statement
           | while_statement
           | thread_statement
           ;

function_definition : function_definition_header function_definition_body END;

function_definition_header : DEF function_name function_definition_params crlf;

function_name : id_function
              | id_
              ;

function_definition_body : expression_list;

result : int_result
       | bool_result
       ;

function_call : function_name LEFT_RBRACKET result RIGHT_RBRACKET
              | function_name LEFT_RBRACKET RIGHT_RBRACKET
              ;

return_statement : RETURN result;

function_definition_params : LEFT_RBRACKET RIGHT_RBRACKET
                           | LEFT_RBRACKET id_ RIGHT_RBRACKET
                           ;

if_statement : IF cond_expression crlf statement_body END
             | IF cond_expression crlf statement_body else_token crlf statement_body END
             ;

while_statement : WHILE cond_expression crlf statement_body END;

thread_statement : THREAD LEFT_CBRACKET crlf statement_body RIGHT_CBRACKET;

cond_expression : bool_result;

statement_body : statement_expression_list;

statement_expression_list : expression terminator
                          | statement_expression_list expression terminator
                          ;

bool_assignment : var_id=id_ op=ASSIGN bool_result;

int_assignment : var_id=id_ op=ASSIGN int_result;

int_result : int_result op=EXP int_result
           | int_result op=( MUL | DIV | MOD ) int_result
           | int_result op=( PLUS | MINUS ) int_result
           | LEFT_RBRACKET int_result RIGHT_RBRACKET
           | int_t
           | id_
           ;

bool_result : bool_result op=AND bool_result
            | bool_result op=OR bool_result
            | LEFT_RBRACKET bool_result RIGHT_RBRACKET
            | NOT bool_result
            | comp_var op=( LESS | GREATER | LESS_EQUAL | GREATER_EQUAL ) comp_var
            | comp_var op=( EQUAL | NOT_EQUAL ) comp_var
            | bool_t
            | id_
            ;

comp_var : int_result
         | id_
         | bool_t
         ;

rvalue : id_

       | int_result
       | bool_result

       | int_assignment
       | bool_assignment

       | bool_t
       | int_t
       ;

int_t : INT;

bool_t : TRUE
       | FALSE
       ;

id_ : ID;

id_global : ID_GLOBAL;

id_function : ID_FUNCTION;

terminator : crlf;

else_token : ELSE;

crlf : CRLF;

CRLF : '\r'? '\n';

END : 'end';
DEF : 'def';
RETURN : 'return';

IF: 'if';
ELSE : 'else';
WHILE : 'while';
THREAD : 'Thread.new';

TRUE : 'true';
FALSE : 'false';

PLUS : '+';
MINUS : '-';
MUL : '*';
DIV : '/';
MOD : '%';
EXP : '**';

EQUAL : '==';
NOT_EQUAL : '!=';
GREATER : '>';
LESS : '<';
LESS_EQUAL : '<=';
GREATER_EQUAL : '>=';

ASSIGN : '=';

AND : '&&';
OR : '||';
NOT : '!';

LEFT_RBRACKET : '(';
RIGHT_RBRACKET : ')';
LEFT_CBRACKET : '{';
RIGHT_CBRACKET : '}';

WS : (' '|'\t')+ -> skip;

INT : [0-9]+;
ID : [a-zA-Z_][a-zA-Z0-9_]*;
ID_GLOBAL : '$'ID;
ID_FUNCTION : ID[?];
