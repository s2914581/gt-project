a = 1 + 2
b = 5 - 3
c = 2 * 2
d = 6 / 3
e = 2 ** 3
f = true == true
g = 5 > 4
h = 5 >= 5
i = 4 < 5
j = 4 <= 4
k = true != false
l = false && false
m = true || false
n = !true
a = 3
if a == 3
  b = 0
end
if a > 2
  b = 1
else
  b = 2
end
while a >= 1
  a = a - 1
end
