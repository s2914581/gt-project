// Generated from Ruby.g4 by ANTLR 4.12.0
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RubyParser}.
 */
public interface RubyListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RubyParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(RubyParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(RubyParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#expression_list}.
	 * @param ctx the parse tree
	 */
	void enterExpression_list(RubyParser.Expression_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#expression_list}.
	 * @param ctx the parse tree
	 */
	void exitExpression_list(RubyParser.Expression_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(RubyParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(RubyParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition(RubyParser.Function_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition(RubyParser.Function_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#function_definition_header}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition_header(RubyParser.Function_definition_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#function_definition_header}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition_header(RubyParser.Function_definition_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#function_name}.
	 * @param ctx the parse tree
	 */
	void enterFunction_name(RubyParser.Function_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#function_name}.
	 * @param ctx the parse tree
	 */
	void exitFunction_name(RubyParser.Function_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#function_definition_body}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition_body(RubyParser.Function_definition_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#function_definition_body}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition_body(RubyParser.Function_definition_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#result}.
	 * @param ctx the parse tree
	 */
	void enterResult(RubyParser.ResultContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#result}.
	 * @param ctx the parse tree
	 */
	void exitResult(RubyParser.ResultContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call(RubyParser.Function_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call(RubyParser.Function_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#return_statement}.
	 * @param ctx the parse tree
	 */
	void enterReturn_statement(RubyParser.Return_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#return_statement}.
	 * @param ctx the parse tree
	 */
	void exitReturn_statement(RubyParser.Return_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#function_definition_params}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition_params(RubyParser.Function_definition_paramsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#function_definition_params}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition_params(RubyParser.Function_definition_paramsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(RubyParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(RubyParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#while_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhile_statement(RubyParser.While_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#while_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhile_statement(RubyParser.While_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#thread_statement}.
	 * @param ctx the parse tree
	 */
	void enterThread_statement(RubyParser.Thread_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#thread_statement}.
	 * @param ctx the parse tree
	 */
	void exitThread_statement(RubyParser.Thread_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#cond_expression}.
	 * @param ctx the parse tree
	 */
	void enterCond_expression(RubyParser.Cond_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#cond_expression}.
	 * @param ctx the parse tree
	 */
	void exitCond_expression(RubyParser.Cond_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#statement_body}.
	 * @param ctx the parse tree
	 */
	void enterStatement_body(RubyParser.Statement_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#statement_body}.
	 * @param ctx the parse tree
	 */
	void exitStatement_body(RubyParser.Statement_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#statement_expression_list}.
	 * @param ctx the parse tree
	 */
	void enterStatement_expression_list(RubyParser.Statement_expression_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#statement_expression_list}.
	 * @param ctx the parse tree
	 */
	void exitStatement_expression_list(RubyParser.Statement_expression_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#bool_assignment}.
	 * @param ctx the parse tree
	 */
	void enterBool_assignment(RubyParser.Bool_assignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#bool_assignment}.
	 * @param ctx the parse tree
	 */
	void exitBool_assignment(RubyParser.Bool_assignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#int_assignment}.
	 * @param ctx the parse tree
	 */
	void enterInt_assignment(RubyParser.Int_assignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#int_assignment}.
	 * @param ctx the parse tree
	 */
	void exitInt_assignment(RubyParser.Int_assignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#int_result}.
	 * @param ctx the parse tree
	 */
	void enterInt_result(RubyParser.Int_resultContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#int_result}.
	 * @param ctx the parse tree
	 */
	void exitInt_result(RubyParser.Int_resultContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#bool_result}.
	 * @param ctx the parse tree
	 */
	void enterBool_result(RubyParser.Bool_resultContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#bool_result}.
	 * @param ctx the parse tree
	 */
	void exitBool_result(RubyParser.Bool_resultContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#comp_var}.
	 * @param ctx the parse tree
	 */
	void enterComp_var(RubyParser.Comp_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#comp_var}.
	 * @param ctx the parse tree
	 */
	void exitComp_var(RubyParser.Comp_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#rvalue}.
	 * @param ctx the parse tree
	 */
	void enterRvalue(RubyParser.RvalueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#rvalue}.
	 * @param ctx the parse tree
	 */
	void exitRvalue(RubyParser.RvalueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#int_t}.
	 * @param ctx the parse tree
	 */
	void enterInt_t(RubyParser.Int_tContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#int_t}.
	 * @param ctx the parse tree
	 */
	void exitInt_t(RubyParser.Int_tContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#bool_t}.
	 * @param ctx the parse tree
	 */
	void enterBool_t(RubyParser.Bool_tContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#bool_t}.
	 * @param ctx the parse tree
	 */
	void exitBool_t(RubyParser.Bool_tContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#id_}.
	 * @param ctx the parse tree
	 */
	void enterId_(RubyParser.Id_Context ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#id_}.
	 * @param ctx the parse tree
	 */
	void exitId_(RubyParser.Id_Context ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#id_global}.
	 * @param ctx the parse tree
	 */
	void enterId_global(RubyParser.Id_globalContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#id_global}.
	 * @param ctx the parse tree
	 */
	void exitId_global(RubyParser.Id_globalContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#id_function}.
	 * @param ctx the parse tree
	 */
	void enterId_function(RubyParser.Id_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#id_function}.
	 * @param ctx the parse tree
	 */
	void exitId_function(RubyParser.Id_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#terminator}.
	 * @param ctx the parse tree
	 */
	void enterTerminator(RubyParser.TerminatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#terminator}.
	 * @param ctx the parse tree
	 */
	void exitTerminator(RubyParser.TerminatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#else_token}.
	 * @param ctx the parse tree
	 */
	void enterElse_token(RubyParser.Else_tokenContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#else_token}.
	 * @param ctx the parse tree
	 */
	void exitElse_token(RubyParser.Else_tokenContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubyParser#crlf}.
	 * @param ctx the parse tree
	 */
	void enterCrlf(RubyParser.CrlfContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubyParser#crlf}.
	 * @param ctx the parse tree
	 */
	void exitCrlf(RubyParser.CrlfContext ctx);
}