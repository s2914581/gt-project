<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="example-logical">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0">
            <attr name="layout">
                <string>50 780 51 72</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>65 688 21 36</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>167 688 57 36</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>158 982 75 72</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>177 872 38 54</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>162 780 68 36</string>
            </attr>
        </node>
        <node id="n9">
            <attr name="layout">
                <string>310 780 68 36</string>
            </attr>
        </node>
        <node id="n11">
            <attr name="layout">
                <string>454 982 75 72</string>
            </attr>
        </node>
        <node id="n12">
            <attr name="layout">
                <string>473 872 38 54</string>
            </attr>
        </node>
        <node id="n13">
            <attr name="layout">
                <string>458 780 68 36</string>
            </attr>
        </node>
        <node id="n14">
            <attr name="layout">
                <string>310 688 68 36</string>
            </attr>
        </node>
        <node id="n15">
            <attr name="layout">
                <string>238 578 104 54</string>
            </attr>
        </node>
        <node id="n16">
            <attr name="layout">
                <string>269 468 41 54</string>
            </attr>
        </node>
        <node id="n17">
            <attr name="layout">
                <string>257 376 66 36</string>
            </attr>
        </node>
        <node id="n18">
            <attr name="layout">
                <string>416 578 43 90</string>
            </attr>
        </node>
        <node id="n20">
            <attr name="layout">
                <string>427 468 22 54</string>
            </attr>
        </node>
        <node id="n21">
            <attr name="layout">
                <string>403 376 69 36</string>
            </attr>
        </node>
        <node id="n22">
            <attr name="layout">
                <string>245 284 90 36</string>
            </attr>
        </node>
        <node id="n23">
            <attr name="layout">
                <string>501 670 59 72</string>
            </attr>
        </node>
        <node id="n25">
            <attr name="layout">
                <string>520 578 21 36</string>
            </attr>
        </node>
        <node id="n26">
            <attr name="layout">
                <string>622 578 57 36</string>
            </attr>
        </node>
        <node id="n27">
            <attr name="layout">
                <string>619 872 71 72</string>
            </attr>
        </node>
        <node id="n29">
            <attr name="layout">
                <string>636 762 38 54</string>
            </attr>
        </node>
        <node id="n30">
            <attr name="layout">
                <string>621 670 68 36</string>
            </attr>
        </node>
        <node id="n31">
            <attr name="layout">
                <string>769 670 54 36</string>
            </attr>
        </node>
        <node id="n33">
            <attr name="layout">
                <string>899 872 75 72</string>
            </attr>
        </node>
        <node id="n34">
            <attr name="layout">
                <string>918 762 38 54</string>
            </attr>
        </node>
        <node id="n35">
            <attr name="layout">
                <string>903 670 68 36</string>
            </attr>
        </node>
        <node id="n36">
            <attr name="layout">
                <string>763 578 68 36</string>
            </attr>
        </node>
        <node id="n37">
            <attr name="layout">
                <string>686 468 104 54</string>
            </attr>
        </node>
        <node id="n38">
            <attr name="layout">
                <string>717 358 41 54</string>
            </attr>
        </node>
        <node id="n39">
            <attr name="layout">
                <string>705 284 66 18</string>
            </attr>
        </node>
        <node id="n40">
            <attr name="layout">
                <string>864 486 43 90</string>
            </attr>
        </node>
        <node id="n41">
            <attr name="layout">
                <string>875 376 22 54</string>
            </attr>
        </node>
        <node id="n42">
            <attr name="layout">
                <string>851 284 69 36</string>
            </attr>
        </node>
        <node id="n43">
            <attr name="layout">
                <string>467 192 90 36</string>
            </attr>
        </node>
        <node id="n44">
            <attr name="layout">
                <string>952 578 52 72</string>
            </attr>
        </node>
        <node id="n46">
            <attr name="layout">
                <string>967 486 21 36</string>
            </attr>
        </node>
        <node id="n47">
            <attr name="layout">
                <string>1069 486 57 36</string>
            </attr>
        </node>
        <node id="n48">
            <attr name="layout">
                <string>1141 578 49 54</string>
            </attr>
        </node>
        <node id="n50">
            <attr name="layout">
                <string>1269 780 71 72</string>
            </attr>
        </node>
        <node id="n51">
            <attr name="layout">
                <string>1286 670 38 54</string>
            </attr>
        </node>
        <node id="n52">
            <attr name="layout">
                <string>1271 578 68 36</string>
            </attr>
        </node>
        <node id="n53">
            <attr name="layout">
                <string>1207 486 68 36</string>
            </attr>
        </node>
        <node id="n54">
            <attr name="layout">
                <string>1094 376 104 54</string>
            </attr>
        </node>
        <node id="n55">
            <attr name="layout">
                <string>1125 266 41 54</string>
            </attr>
        </node>
        <node id="n56">
            <attr name="layout">
                <string>1113 192 66 18</string>
            </attr>
        </node>
        <node id="n57">
            <attr name="layout">
                <string>1272 394 43 90</string>
            </attr>
        </node>
        <node id="n58">
            <attr name="layout">
                <string>1283 284 22 54</string>
            </attr>
        </node>
        <node id="n59">
            <attr name="layout">
                <string>1259 192 69 36</string>
            </attr>
        </node>
        <node id="n60">
            <attr name="layout">
                <string>650 82 90 54</string>
            </attr>
        </node>
        <node id="n61">
            <attr name="layout">
                <string>680 8 29 18</string>
            </attr>
        </node>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:ID</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:text = string:"l"</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Id_</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n2" to="n0">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n2" to="n3">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:ASSIGN</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:text = string:"="</string>
            </attr>
        </edge>
        <edge from="n3" to="n14">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>type:FALSE</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:text = string:"false"</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:Bool_t</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n7" to="n5">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n8" to="n9">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n8" to="n7">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>type:AND</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>let:text = string:"&amp;&amp;"</string>
            </attr>
        </edge>
        <edge from="n9" to="n13">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>type:FALSE</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>let:text = string:"false"</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>type:Bool_t</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n12" to="n11">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n13" to="n12">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n14" to="n8">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n14" to="n13">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n14" to="n9">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>type:Bool_assignment</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n15" to="n14">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n15" to="n2">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n15" to="n3">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>type:Rvalue</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n16" to="n15">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>type:Expression</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n17" to="n16">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n17" to="n21">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>type:CRLF</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>let:text = string:"&#13;
"</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>type:Crlf</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n20" to="n18">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>type:Terminator</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n21" to="n20">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>type:Expression_list</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n22" to="n17">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n22" to="n39">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n22" to="n21">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>type:ID</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>let:text = string:"m"</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>type:Id_</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n25" to="n26">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n25" to="n23">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>type:ASSIGN</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:text = string:"="</string>
            </attr>
        </edge>
        <edge from="n26" to="n36">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>type:TRUE</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>let:text = string:"true"</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>type:Bool_t</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n29" to="n27">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n30" to="n29">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n30" to="n31">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>type:OR</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>let:text = string:"||"</string>
            </attr>
        </edge>
        <edge from="n31" to="n35">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>type:FALSE</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>let:text = string:"false"</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>type:Bool_t</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n34" to="n33">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n35" to="n34">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n36" to="n35">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n36" to="n31">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n36" to="n30">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>type:Bool_assignment</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n37" to="n36">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n37" to="n25">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n37" to="n26">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>type:Rvalue</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n38" to="n37">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>type:Expression</string>
            </attr>
        </edge>
        <edge from="n39" to="n38">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n39" to="n42">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>type:CRLF</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>let:text = string:"&#13;
"</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>type:Crlf</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n41" to="n40">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>type:Terminator</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n42" to="n41">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>type:Expression_list</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n43" to="n42">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n43" to="n22">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n43" to="n39">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n43" to="n56">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>type:ID</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>let:text = string:"n"</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>type:Id_</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n46" to="n44">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n46" to="n47">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>type:ASSIGN</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>let:text = string:"="</string>
            </attr>
        </edge>
        <edge from="n47" to="n53">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>type:NOT</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>let:text = string:"!"</string>
            </attr>
        </edge>
        <edge from="n48" to="n52">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>type:TRUE</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>let:text = string:"true"</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>type:Bool_t</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n51" to="n50">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n52" to="n51">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>type:Bool_result</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n53" to="n52">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n53" to="n48">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>type:Bool_assignment</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n54" to="n47">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n54" to="n53">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n54" to="n46">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n55" to="n55">
            <attr name="label">
                <string>type:Rvalue</string>
            </attr>
        </edge>
        <edge from="n55" to="n55">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n55" to="n55">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n55" to="n54">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>type:Expression</string>
            </attr>
        </edge>
        <edge from="n56" to="n59">
            <attr name="label">
                <string>next</string>
            </attr>
        </edge>
        <edge from="n56" to="n55">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>type:CRLF</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>let:text = string:"&#13;
"</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>type:Crlf</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n58" to="n57">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>type:Terminator</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n59" to="n58">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>type:Expression_list</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>flag:first</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>flag:last</string>
            </attr>
        </edge>
        <edge from="n60" to="n59">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n60" to="n43">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n60" to="n56">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>type:Prog</string>
            </attr>
        </edge>
        <edge from="n61" to="n60">
            <attr name="label">
                <string>child</string>
            </attr>
        </edge>
    </graph>
</gxl>
